# Debian

[Debian](https://www.debian.org/) GNU/Linux ist eine gemeinschaftgetriebene Distribuition die das Ziel verfolgt, nur aus freie, gut getestete stabile und sichere Software zu bestehen. Dadurch ist sie sehr resourcenschonend und bittet somit die nötige Zuverlässigkeit um Server zu betreiben. Genauso die Flexiblität um es zu **dein** Betriebssytem zu machen, für dein/e PC, Laptop, Server, Raspberry Pi, CNC-Maschine oder Embedded-Systeme, denn Debian gibt es auf viele verschiedene [Hardware-Architekturen](#hardware-architekturen).

## Getting started

Diese Anleitung hilft Ihnen dabei das Wissen zu erlangen um das Betriebssystem **Debian** auf ihren Rechner zu installieren. Es wird auch gezeigt wie man Anwendungen/Programme installieren und deinstallieren kann.

Seien Sie sich bewusst dass bei eine Betriebssysteminstallation alle Daten auf der Festplatte gelöscht werden! Sie können die nächste Schritte auch testweise in eine VM([Virtuelle Maschine](https://de.wikipedia.org/wiki/Virtualisierung_%28Informatik%29)) ausführen.

Da Debian ausschließlich aus freie Software besteht, kann es zu Einschränkungen bei der Nutzung der Hardware oder abspielen von Medien kommen. Wenn sie es bevorzugen, die vom Hardwarehersteller angebotene (unfreie) Drittanbieter-Firm- und Software, sowie Video-Codecs zu benutzen um die Einschränkungen umzugehen können sie dies [tun](#unfreie-software).

Sie benötigen für die nächsten Schritte ein leeres mindestens 512MB großes Speichermedium (z.B. USB-stick, CD/DVD) um ein Boot-fähiger Installationsgerät zu erstellen, dafür brauchen Sie noch ein Programm wie [Rufus](https://rufus.ie), [balenaEtecher](https://www.balena.io/etcher/), [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) oder auf CD/DVD mit [Brasero](https://wiki.gnome.org/Apps/Brasero), der Ihnen dabei hilft den Startfähigen Installationsgerät zu erstellen.

Als nächstes benötigen Sie das [Installations-Abbild](https://www.debian.org/download), diese bringgen Sie dann mithilfe des vorher installierten Programms auf dem Speichermedium.

Danach geht es weiter mit der [Installation](#installation).

Wenn sie weitere Hilfe/Infos benötigen, haben wir eine Liste weiterführende [Links](#links)

## Hardware Architekturen

Bekannteste Debian-[Portierungen](https://www.debian.org/ports/)
* amd64
* arm64
* armhf
* i386
* mips
* s390x
* powerpc
* sparc


## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Unfreie Software

Wenn die WLAN-Karte nicht richtig funtioniert, oder ihr Browser keine Medien abspielt, könnte es daran liegen, dass die dazu nötigen Treiber oder Codecs fehlen da sie nicht **frei** sind. Diese können nachinstalliert werden:

Dazu öffnen wir einen Terminal...


## Links

[Download](https://www.debian.org/download)
[Portierungen](https://www.debian.org/ports/)

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
For open source projects, say how it is licensed.
